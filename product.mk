TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := arm

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-musleabihf/bin/arm-linux-musleabihf-
TARGET_UBOOT_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-gnueabihf/bin/arm-linux-gnueabihf-
TARGET_LINUX_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-gnueabihf/bin/arm-linux-gnueabihf-

TARGET_GLOBAL_CFLAGS += -march=armv7-a

TARGET_LINUX_IMAGE := zImage

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TARGET_VENDOR_DIR)/skel

TARGET_LINUX_DEVICE_TREE :=
TARGET_LINUX_DEVICE_TREE_NAMES := mt7623n-bananapi-bpi-r2.dtb \
	$(NULL)

UBOOT_CONFIG_TARGET := mt7623n_bpir2_defconfig
TARGET_UBOOT_CONFIG_MERGE_FILES := yes
